!function($) {

	'use strict';

	$(document).ready(function() {
		$('html').addClass('mn-ready');

		if ($('#mn-map').length) {
			loadGoogleMaps();
		}
	});

}(jQuery);

function loadGoogleMaps() {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = '//maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&callback=initializeGoogleMaps';
	document.body.appendChild(script);
}

function initializeGoogleMaps() {
	var el = document.getElementById('mn-map');
	var lat = el.dataset.lat || alert('Erro no Google Maps: Latitude indefinida.');
	var lng = el.dataset.lng || alert('Erro no Google Maps: Longitude indefinida.');
	var zoom = el.dataset.zoom || 15;
	var title = el.dataset.title;
	var styledMap = new google.maps.StyledMapType([
		{
			stylers: [
				{ saturation: -100 }
			]
		},{
			featureType: 'road',
			elementType: 'geometry',
			stylers: [
				{ lightness: 10 },
				{ visibility: 'simplified' }
			]
		},{
			featureType: 'road',
			elementType: 'labels',
			stylers: [
				{ visibility: 'off' }
			]
		}
	]);
	var mapOptions = {
		zoom: zoom,
		disableDefaultUI: true,
		streetViewControl: true,
		streetViewControlOptions: {
			position: google.maps.ControlPosition.RIGHT_CENTER
		},
		zoomControl: true,
		zoomControlOptions: {
			position: google.maps.ControlPosition.RIGHT_CENTER
		},
		scrollwheel: false,
		center: new google.maps.LatLng(lat, lng),
		mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'mapStyle']
		}
	};
	var map = new google.maps.Map(el, mapOptions);

	map.mapTypes.set('mapStyle', styledMap);
	map.setMapTypeId('mapStyle');

	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat, lng),
		map: map,
		title: title,
		animation: google.maps.Animation.DROP
	});
}
<?php

$base        = __DIR__ . '/..';
$common      = __DIR__ . '/../../common';
$environment = APP_ENVIRONMENT;
$params      = require(__DIR__ . '/params.php');

return CMap::mergeArray(require($common . '/config/main.php'), array(
	'components' => array(
		'urlManager' => array(
			'urlFormat' => 'path',
			'showScriptName' => false, // requer .htaccess
			'rules' => array(
				'sobre' => 'home/about',
				'contato' => 'home/contact',
				'ambientes/<category>' => 'home/products',
				'ambientes' => 'home/products',
				'perguntas-frequentes' => 'home/faq',
			),
		),
	),

	'params' => $params,
), require(__DIR__ . '/main-' . $environment . '.php'));
<?php

$params = require(__DIR__ . '/params-local.php');

return array(
	'modules' => array(
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => 'admin',
			'generatorPaths' => array(
				'ext.giix-core', // giix generators
			),
		),
	),

	'params' => $params,
);
<?php

class HomeController extends Controller
{
	public function actionIndex() 
	{
		$this->render('index');
	}

	public function actionProducts($category = null, $id = null) 
	{
		$categories = $this->productCategories;

		if ($category === null)
			$this->redirect(array('products', 'category' => $this->currentProductCategory->alias));

		foreach ($categories as $item) {
			if ($item->alias == $category) {
				$currentCategory = $item;
				break;
			}
		}

		if (empty($currentCategory)) {
			$currentCategory = Category::model()->findByAttributes(array('alias' => $category));

			if ($currentCategory === null)
				throw new CHttpException(404, 'Página não encontrada.');
		} else
			$this->currentProductCategory = $currentCategory;

		$products = $currentCategory->products;

		$this->render('products', array(
			'currentCategory' => $currentCategory,
			'categories' => $categories, 
			'products' => $products, 
		));
	}

	public function actionAbout() 
	{
		$this->render('about');
	}

	public function actionFaq() 
	{
		$this->render('faq', array('tips' => Tip::model()->published()->findAll()));
	}

	public function actionContact() 
	{
		$contactForm = new ContactForm;
		
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'contactForm') {
			echo CActiveForm::validate($contactForm);
			Yii::app()->end();
		}
		
		if (isset($_POST['ContactForm'])) {
			$contactForm->attributes = $_POST['ContactForm'];

			if ($contactForm->validate() && $contactForm->process()) {
				Yii::app()->user->setAlert(WebUser::ALERT_SUCCESS, 'Seu solicitação foi enviado com sucesso. Por favor, aguarde nosso contato.');

				if (isset($_POST['returnUrl']))
					$this->redirect($_POST['returnUrl']);
				else
					$this->redirect(array('index'));
			} else
				Yii::app()->user->setAlert(WebUser::ALERT_DANGER, current(current($contactForm->getErrors())));
		}

		$this->render('contact', array('contactForm' => $contactForm));
	}

	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}


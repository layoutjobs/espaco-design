<?php

class ContactForm extends CFormModel
{
	public $name;
	public $email;
	public $subject;
	public $message;

	public function rules()
	{
		return array(
			array('name, email, subject, message', 'required'),
			array('email', 'email'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'name' => 'Nome',
			'email' => 'E-mail',
			'subject' => 'Assunto',
			'message' => 'Mensagem',
		);
	}

	public function getSubjectOptions()
	{
		return array(
			1 => 'Dúvidas',
			2 => 'Sugestões',
			3 => 'Envio de Currículo',
		);
	}

	public function process()
	{
		$message = new YiiMailMessage;
		$message->setTo(Yii::app()->params['mail.senderEmail']);
		$message->setFrom(Yii::app()->params['mail.senderEmail']);
		$message->setReplyTo($this->email);
		$body = Yii::app()->controller->renderPartial(
			'application.views.mail.internal.contact', 
			array('contactForm' => $this, 'message' => $message), true
		);	
		$message->setBody(array('message' => $message, 'content' => $body), 'text/html');

		if (!Yii::app()->mail->send($message)) {
			$this->addError(null, 'Erro inesperado ao enviar a mensagem, por favor, tente novamente mais tarde.');
			return false;
		} else
			return true;
	}
}
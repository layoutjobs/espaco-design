<!-- The file upload form used as target for the file upload widget -->
<form action="<?php echo $this->url; ?>" method="POST" enctype="multipart/form-data">
    <!-- Redirect browsers with JavaScript disabled to the origin page -->
    <noscript><input type="hidden" name="redirect" value="<?php echo Yii::app()->request->getUrlReferrer(); ?>"></noscript>
    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
    <div class="fileupload-buttonbar">
        <span class="btn btn-sm btn-primary fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span>Adicionar arquivos...</span>
            <input type="file" name="files[]" multiple>
        </span>
    </div>
    <!-- The table listing the files available for upload/download -->
    <table role="presentation" class="table"><tbody class="files"></tbody></table>
</form>
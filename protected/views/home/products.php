<?php 
$this->pageTitle = $currentCategory->name;
?>

<div class="uk-grid" data-uk-grid-margin>

	<?php foreach ($products as $product) : ?>

	<div class="uk-width-small-1-2 uk-width-large-1-3">
		<div class="mn-cover mn-cover-bg mn-cover-16by9 uk-overlay" style="background-image: url('<?php echo $product->getImage('small')->url; ?>');">    
	
			<img src="<?php echo $product->getImage('small')->url; ?>" alt="<?php echo CHtml::encode($product->name); ?>">

			<div class="uk-overlay-area">
				<div class="uk-overlay-area-content">
					<h3><?php echo CHtml::encode($product->name); ?></h3>			
					<?php if ($product->manufacturer) : ?>
					<p><strong>Fabricante:</strong> <?php echo CHtml::encode($product->manufacturer->name); ?></p>
					<?php endif; ?>
					<?php echo $product->description; ?>
					<a class="uk-button uk-button-glass" href="<?php echo $product->getImage('large')->url; ?>"  title="<?php echo CHtml::encode($product->name); ?>" data-uk-lightbox="{group:'product'}"><i class="uk-icon-search"></i></a>
				</div>
			</div>                                   
		</div>
	</div>

	<?php endforeach; ?>

</div>       

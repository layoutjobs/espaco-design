<?php 
$this->mainAlt = true;
$this->pageTitle = 'Contato';
$this->mainTitle = 'Fale Conosco';
$this->mainHeaderCover = $this->renderPartial('_contactHeaderCover', null, true);
?>

<p>Venha tomar um café conosco, teremos muito prazer em poder ouvir a sua ideia.</p>		
<p class="uk-margin-large-bottom">
	Rua Rui Barbosa, 1553, Centro, CEP: 13320-230 - Salto/SP<br>
	<strong>Telefone: <?php echo CHtml::encode(Yii::app()->params['biz.phone']); ?></strong> &ndash; <strong>Celular: <?php echo CHtml::encode(Yii::app()->params['biz.mobile']); ?></strong>
</p>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'contactForm',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'uk-form uk-form-stacked',
	),
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
)); ?>

<div class="uk-grid uk-grid-small uk-text-center" data-uk-grid-margin>
	<div class="uk-width-1-1">
		<?php echo $form->dropDownList($contactForm, 'subject', $contactForm->subjectOptions, array('class' => 'uk-width-1-1 uk-form-large')); ?>
		<?php echo $form->error($contactForm, 'subject', array('class' => 'uk-text-danger uk-text-small uk-text-bold uk-text-italic uk-margin-small-top')); ?>
	</div>
	<div class="uk-width-1-1 uk-width-medium-1-2">
		<?php echo $form->textField($contactForm, 'name', array('class' => 'uk-width-1-1 uk-form-large', 'placeholder' => $contactForm->getAttributeLabel('name'))); ?>
		<?php echo $form->error($contactForm, 'name', array('class' => 'uk-text-danger uk-text-small uk-text-bold uk-text-italic uk-margin-small-top')); ?>
	</div>				
	<div class="uk-width-1-1 uk-width-medium-1-2">
		<?php echo $form->textField($contactForm, 'email', array('class' => 'uk-width-1-1 uk-form-large', 'placeholder' => $contactForm->getAttributeLabel('email'))); ?>
		<?php echo $form->error($contactForm, 'email', array('class' => 'uk-text-danger uk-text-small uk-text-bold uk-text-italic uk-margin-small-top')); ?>
	</div>					
	<div class="uk-width-1-1">
		<?php echo $form->textArea($contactForm, 'message', array('class' => 'uk-width-1-1 uk-form-large', 'placeholder' => $contactForm->getAttributeLabel('message'), 'rows' => 4)); ?>
		<?php echo $form->error($contactForm, 'message', array('class' => 'uk-text-danger uk-text-small uk-text-bold uk-text-italic uk-margin-small-top')); ?>
	</div>
	<div class="uk-width-1-1">
		<button type="submit" class="uk-button uk-button-primary uk-button-large"><i class="uk-icon-check"></i> Enviar</button>
	</div>
</div>		
 
<?php $this->endWidget(); ?>

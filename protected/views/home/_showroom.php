<div class="mn-showroom uk-slidenav-position" data-uk-slideshow="{start:<?php echo $currentProductCategoryIndex; ?>, autoplay:true}">
    <ul class="uk-slideshow">

        <?php foreach ($productCategories as $i => $category) : ?> 

        <li<?php echo $currentProductCategoryIndex == $i ? ' class="uk-active"' : ''; ?> style="background-image: url('<?php echo APP_IMG_URL . '/covers/category-' . $category->alias . '.jpg'; ?>');">
            <div class="mn-showroom-content">
                <h1 class="mn-showroom-title"><?php echo CHtml::encode($category->name); ?></h1>
                <p><?php echo CHtml::encode($category->description); ?></p>
                <a class="uk-button uk-button-glass" href="<?php echo $this->createUrl('home/products', array('category' => $category->alias)); ?>">Mais <?php echo CHtml::encode($category->name); ?></a>
            </div>
        </li>

        <?php endforeach; ?>

    </ul>

    <?php echo $this->renderPartial('_showroomNav', array(
        'productCategories' => $productCategories, 
        'currentProductCategory' => $currentProductCategory,
        'currentProductCategoryIndex' => $currentProductCategoryIndex,
    )); ?>   
    
</div>
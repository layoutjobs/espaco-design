<ul class="mn-showroom-nav">
	<?php foreach ($productCategories as $i => $category) : ?> 

	<li<?php echo $i == $currentProductCategoryIndex ? ' class="uk-active"' : ''; ?> data-uk-slideshow-item="<?php echo $i; ?>">
		<a href="<?php echo $this->createUrl('home/products', array('category' => $category->alias)); ?>" title="<?php echo CHtml::encode($category->name); ?>"  data-uk-tooltip style="background-image: url('<?php echo $category->getImage('small')->url; ?>');">
			<div class="uk-overlay">
				<div class="uk-overlay-area">
					<div class="uk-overlay-area-content">
						<h3 class="mn-showroom-nav-title"><?php echo CHtml::encode($category->name); ?></h3>
					</div>
				</div>
			</div>
		</a>
	</li>

	<?php endforeach; ?>
</ul>
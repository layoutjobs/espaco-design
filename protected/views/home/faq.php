<?php 
$this->pageTitle = 'Dicas';
$this->mainAlt = true;
$this->mainTitle = 'Perguntas Frequentes';
$this->mainHeaderCover = $this->renderPartial('_faqHeaderCover', null, true);
?>

<p class="uk-margin-large-bottom">Abaixo estão algumas perguntas frequentes, caso tenha mais dúvidas por favor entre em contato.</p>

<div class="uk-accordion uk-grid uk-text-left" data-uk-accordion>
	<?php foreach ($tips as $tip) : ?>
	
	<div class="uk-width-1-2">
	  <h3 class="uk-accordion-title"><i class="uk-icon-plus-minus uk-icon-bordered"></i> <?php echo CHtml::encode($tip->title); ?></h3>
	  <div class="uk-accordion-content"><?php echo $tip->content; ?></div>
	</div>

  <?php endforeach; ?>
</div>

<?php 
$this->pageTitle = 'Sobre';
$this->mainAlt = true;
$this->mainTitle = 'A Espaço Design';
$this->mainHeaderCover = $this->renderPartial('_aboutHeaderCover', null, true);
?>

<div class="uk-grid uk-text-justify" data-uk-grid-margin>
    <div class="uk-width-1-1 uk-width-medium-1-2">
        <p>Espaço Design reconhecida pela sua qualidade em móveis planejados, conta com um know-how de 10 anos de mercado. Uma marca que oferece aos seus clientes o que há de mais atual em design e decoração.</p>
        <p>Com sua experiência, a Espaço Design sabe que cada cliente tem sua necessidade exclusiva, por isso valoriza em todos seus projetos a identidade e a expectativa de cada cliente. Proporcionando assim não apenas móveis, mas ambientes aconchegantes, exclusivos e funcionais.</p>
    </div>
    <div class="uk-width-1-1 uk-width-medium-1-2">
        <p>A Espaço Design trabalha com matéria-prima ecologicamente correta, garantia e prazo de entrega combinado em contrato. Desta forma, garante total satisfação aos seus clientes, através de um atendimento diferenciado e ideias inovadoras, proporcionando ambientes residenciais ou comerciais de bom gosto e com o melhor aproveitamento do espaço.</p>
        <p>Tome um café com o pessoal da Espaço Design e fale sobre o seu projeto, você irá se surpreender com as soluções que poderão ajudar você a realizar seu sonho.</p>
    </div>
</div>      

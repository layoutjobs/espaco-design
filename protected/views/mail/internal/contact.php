<?php 
$message->view = 'internal'; 
$message->subject = 'Contato - ' . CHtml::encode($contactForm->name); 
?>

<p>Olá.</p>

<p><b><?php echo CHtml::encode($contactForm->name); ?></b> entrou em contato pelo site.</p>

<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
	<tbody>

		<?php foreach ($contactForm->attributes as $attributeName => $value) : ?>

		<?php
		if ($value && method_exists($contactForm, ('get' . ucfirst($attributeName) . 'Options'))) 
			$value = $contactForm->{$attributeName . 'Options'}[$value];
		?>

		<tr>
			<th align="right"><?php echo CHtml::encode($contactForm->getAttributeLabel($attributeName)); ?></th>
			<td width="5%"></td>
			<td><?php echo CHtml::encode($value); ?></td>
		</tr>

		<?php endforeach; ?>

	</tbody>
</table>
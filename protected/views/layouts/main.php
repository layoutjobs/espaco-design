<!DOCTYPE html>
<html lang="pt-br" class="<?php echo 'pg-' . $this->id . '-' . $this->action->id . ($this->fullscreen ? ' mn-fullscreen' : ''); ?>">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php echo CHtml::encode(Yii::app()->params['site.description']); ?>">
		<meta name="keywords" content="<?php echo CHtml::encode(Yii::app()->params['site.keywords']); ?>">

		<title><?php echo CHtml::encode(($this->pageTitle ? $this->pageTitle . ' - ' : null) . Yii::app()->params['site.name']); ?></title>

		<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/uikit.css'); ?>
		<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/main.css'); ?>	

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<link href="http://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
		<link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,400,400italic,700,700italic" rel="stylesheet">
	</head>
	<body>

		<div class="mn-wrapper">
			<div class="mn-cover-bg" style="background-image: url('<?php echo APP_IMG_URL . '/covers/category-' . $this->currentProductCategory->alias . (!$this->fullscreen ? '-blur' : '') . '.jpg'; ?>')"></div>

			<header class="mn-header uk-clearfix">
				<div class="mn-header-offcanvas-toggle uk-hidden-large">
					<a class="uk-icon-button uk-icon-bars" href="#offcanvas" data-uk-offcanvas></a>
				</div>
				<nav class="mn-header-nav uk-visible-large">
					<ul>
						<li<?php echo $this->id === 'home' && $this->action->id === 'index' ? ' class="mn-active"' : '' ?>>
							<a href="<?php echo Yii::app()->homeUrl; ?>">Home</a>
						</li>
						<li<?php echo $this->id === 'home' && $this->action->id === 'products' ? ' class="mn-active"' : '' ?>>
							<a href="<?php echo Yii::app()->createUrl('/home/products'); ?>">Ambientes</a>
						</li>
						<li<?php echo $this->id === 'home' && $this->action->id === 'about' ? ' class="mn-active"' : '' ?>>
							<a href="<?php echo Yii::app()->createUrl('/home/about'); ?>">Sobre</a>
						</li>
						<li<?php echo $this->id === 'home' && $this->action->id === 'faq' ? ' class="mn-active"' : '' ?>>
							<a href="<?php echo Yii::app()->createUrl('/home/faq'); ?>">Dicas</a>
						</li>
						<li<?php echo $this->id === 'home' && $this->action->id === 'contact' ? ' class="mn-active"' : '' ?>>
							<a href="<?php echo Yii::app()->createUrl('/home/contact'); ?>">Contato</a>
						</li>
					</ul>                
				</nav>
			</header>


			<main class="mn-body uk-clearfix">

				<?php if (!$this->mainAlt) : ?>
				
				<div class="mn-top">
					<a class="mn-brand" href="<?php echo Yii::app()->homeUrl; ?>">
						<img src="<?php echo APP_IMG_URL; ?>/logo.png" alt="<?php echo CHtml::encode(Yii::app()->params['site.name']); ?>">
					</a>
				</div>

				<?php endif; ?>
				
				<div class="<?php echo $this->mainAlt ? 'mn-main-alt' : 'mn-main'; ?>">

					<?php if ($this->fullscreen) : ?>

					<?php echo $content; ?>

					<?php else : ?>

					<a class="mn-main-brand mn-brand mn-brand-compact" href="<?php echo Yii::app()->homeUrl; ?>">
						<img src="<?php echo APP_IMG_URL; ?>/logo-compact.png" alt="<?php echo CHtml::encode(Yii::app()->params['site.name']); ?>">
					</a>
					<a class="mn-main-close mn-close" href="<?php echo Yii::app()->homeUrl; ?>"></a>
					<header class="mn-main-header">
						<div class="uk-container uk-container-center">

							<?php if ($this->mainHeaderCover) : ?>

							<div class="mn-main-cover mn-cover">
								<?php echo $this->mainHeaderCover; ?>
							</div>

							<?php endif; ?>

							<h1 class="mn-main-header-title"><?php echo $this->mainTitle ? $this->mainTitle : CHtml::encode($this->pageTitle); ?></h1>  

							<?php if ($this->id == 'home' && $this->action->id == 'products') : ?>

							<nav class="mn-main-header-nav uk-visible-large">
								<ul>

									<?php foreach ($this->productCategories as $i => $category) : ?> 

									<li<?php echo $this->currentProductCategoryIndex == $i ? ' class="mn-active"' : ''; ?>>
										<a href="<?php echo $this->createUrl('/home/products', array('category' => $category->alias)); ?>"><?php echo CHtml::encode($category->name); ?></a>
									</li>

									<?php endforeach; ?>

								</ul>
							</nav>			

							<?php endif; ?>

						</div>
					</header>
					<div class="mn-main-body">
						<div class="uk-container uk-container-center">
							
							<?php echo $content; ?>

						</div>
					</div>

					<?php endif; ?>

				</div>

				<?php if (!$this->mainAlt) : ?>

				<div class="mn-bottom">               
					<h2 class="mn-bottom-title">Ambientes</h2>
					<?php echo $this->renderPartial('/home/_showroomNav', array(
						'productCategories' => $this->productCategories, 
						'currentProductCategory' => $this->currentProductCategory, 
						'currentProductCategoryIndex' => $this->currentProductCategoryIndex,
					)); ?>   
				</div>

				<?php endif; ?>
			</main>


			<footer class="mn-footer uk-clearfix">
				<div class="uk-container uk-container-center">
						<div class="mn-row">
							<div class="mn-col mn-col-a">Fale conosco: <?php echo CHtml::encode(Yii::app()->params['biz.phone'] . (Yii::app()->params['biz.mobile'] ? ' / ' . Yii::app()->params['biz.mobile'] : '')); ?></div>
							<div class="mn-col mn-col-b">
								<?php if (Yii::app()->params['facebook.url']) : ?>
									<a class="uk-icon-button uk-icon-button-glass uk-icon-facebook uk-margin-left uk-margin-right" href="<?php echo Yii::app()->params['facebook.url']; ?>" target="_blank"></a>
								<?php endif; ?>
							</div>
							<div class="mn-col mn-col-c">&copy; <?php echo date('Y'); ?>. <?php echo CHtml::encode(Yii::app()->params['biz.name']); ?>.</div>
						</div>
				</div>
			</footer>   
		</div>

		<div id="offcanvas" class="uk-offcanvas">
			<div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
				<ul class="uk-nav uk-nav-offcanvas">
					<li><a href="<?php echo Yii::app()->homeUrl; ?>">Home</a></li>
					<li>
						<a href="<?php echo Yii::app()->createUrl('/home/products'); ?>">Ambientes</a>
						<div>
							<ul class="uk-nav-sub">
								<?php foreach ($this->productCategories as $i => $category) : ?> 

								<li><a href="<?php echo $this->createUrl('/home/products', array('category' => $category->alias)); ?>"><?php echo CHtml::encode($category->name); ?></a></li>

								<?php endforeach; ?>
							</ul>
						</div>
					</li>
					<li><a href="<?php echo Yii::app()->createUrl('home/about'); ?>">Sobre</a></li>
					<li><a href="<?php echo Yii::app()->createUrl('home/faq'); ?>">Dicas</a></li>
					<li><a href="<?php echo Yii::app()->createUrl('home/contact'); ?>">Contato</a></li>
				</ul>
			</div>
		</div>


		<div class="mn-flahes">
			<ul>
				<!--[if lt IE 9]>
				<li class="uk-text-danger"><i class="uk-icon-times-circle"></i> Você está usando um navegador <strong>desatualizado</strong>. Por favor, atualize seu navegador para melhorar sua experiência.</li>
				<![endif]-->

				<noscript>
				<li class="uk-text-danger"><i class="uk-icon-times-circle"></i> Por favor, ative o javascript no seu navegador.</li>
				</noscript>

				<?php if ($alert = Yii::app()->user->getAlert(WebUser::ALERT_SUCCESS)) : ?>
			
				<li class="uk-text-success"><i class="uk-icon-check-circle"></i> <strong>Sucesso!</strong> <?php echo $alert; ?></li>
			
				<?php endif; ?>

				<?php if ($alert = Yii::app()->user->getAlert(WebUser::ALERT_INFO)) : ?>
			
				<li class="uk-text-info"><i class="uk-icon-exclamation-circle"></i> <strong>Info!</strong> <?php echo $alert; ?></li>
			
				<?php endif; ?>

				<?php if ($alert = Yii::app()->user->getAlert(WebUser::ALERT_WARNING)) : ?>
			
				<li class="uk-text-warning"><i class="uk-icon-exclamation-circle"></i> <strong>Atencão!</strong> <?php echo $alert; ?></li>
			
				<?php endif; ?>

				<?php if ($alert = Yii::app()->user->getAlert(WebUser::ALERT_DANGER)) : ?>
			
				<li class="uk-text-danger"><i class="uk-icon-times-circle"></i> <strong>Ops. Algo deu errado.</strong> <?php echo $alert; ?></li>

				<?php endif; ?>
			</ul>
		</div>


		<?php if (Yii::app()->params['ga.trackingId']) : ?>

		<script>
		var _gaq=[['_setAccount','<?php echo Yii::app()->params['ga.trackingId'] ?>'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src='//www.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
		</script>

		<?php endif; ?>

		<?php Yii::app()->clientScript->registerCoreScript('jquery', CClientScript::POS_END); ?>
		<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor/uikit/dist/js/uikit.min.js', CClientScript::POS_END); ?>
		<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor/uikit/dist/js/components/lightbox.min.js', CClientScript::POS_END); ?>
		<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor/uikit/dist/js/components/slideshow.min.js', CClientScript::POS_END); ?>
		<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/vendor/uikit/dist/js/components/accordion.min.js', CClientScript::POS_END); ?>
		<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/main.js', CClientScript::POS_END); ?>
		
	</body>
</html>
<?php

class Controller extends CommonController
{
	public $fullscreen = false;

	public $mainAlt = false;
	public $mainTitle;
	public $mainHeaderCover;

	private $_productCategories = array();
	private $_currentProductCategory;
	private $_currentProductCategoryIndex;
	
	public function getProductCategories()
	{
		if (!$this->_productCategories) {
			$productCategories = Category::model()->of('products')->hasParent()->findAll(array('order' => 't.sequence'));
			$productCategoriesAdd = array();

			foreach ($productCategories as $category) {
				if ($category->image) 
					$productCategoriesAdd[] = $category;
			}

			$this->_productCategories = $productCategoriesAdd;
		}

		return $this->_productCategories;
	}

	public function setCurrentProductCategory($productCategory)
	{
		$productCategoryIndex = $productCategory->id;

		foreach ($this->productCategories as $i => $productCategory) {
			if ($productCategory->id == $productCategoryIndex) {
				$productCategoryIndex = $i;
				break;
			}
		}

		$this->_currentProductCategory = $productCategory;
		$this->_currentProductCategoryIndex = $productCategoryIndex;
		Yii::app()->user->setState('controller.currentProductCategoryIndex', $productCategoryIndex);
	}

	public function getCurrentProductCategory()
	{
		if ($this->_currentProductCategory === null) {
			if (!$this->productCategories)
				return;			

			$productCategoryIndex = null;

			if (Yii::app()->user->hasState('controller.currentProductCategoryIndex', false)) {
				$productCategoryIndex = Yii::app()->user->getState('controller.currentProductCategoryIndex');
			}

			while (!isset($this->productCategories[$productCategoryIndex])) {
				$productCategoryIndex = mt_rand(0, count($this->productCategories));
			}

			$productCategory = $this->productCategories[$productCategoryIndex];
			$this->_currentProductCategory = $productCategory;
		}

		return $this->_currentProductCategory;
	}

	public function getCurrentProductCategoryIndex()
	{
		return $this->_currentProductCategoryIndex;
	}
}

<?php

class Controller extends CommonController
{	
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('error', 'login'),
				'users' => array('*'),
			),
			array(
				'allow',
				'actions' => array('logout'),
				'users' => array('@'),
			),
			array(
				'allow',
				'expression' => '$user->is(\'admin\')',
			),
			array('deny'),
		);
	}
}

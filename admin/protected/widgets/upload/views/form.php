<!-- The file upload form used as target for the file upload widget -->
<form action="<?php echo $this->url; ?>" method="POST" enctype="multipart/form-data">
    <!-- Redirect browsers with JavaScript disabled to the origin page -->
    <noscript><input type="hidden" name="redirect" value="<?php echo Yii::app()->request->getUrlReferrer(); ?>"></noscript>
    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
    <div class="fileupload-buttonbar clearfix">
        <span class="btn btn-sm btn-primary fileinput-button">
            <i class="icon-plus"></i>
            <span>Adicionar arquivos...</span>
            <input type="file" name="files[]" multiple>
        </span>
        <button type="submit" class="btn btn-sm start">
            <i class="icon-upload"></i>  
        </button>
        <button type="reset" class="btn btn-sm cancel">
            <i class="icon-ban-circle"></i>
        </button>
        <button type="button" class="btn btn-sm delete">
            <i class="icon-trash"></i>
        </button>
        <input type="checkbox" class="toggle">
        <!-- The global file processing state -->
        <span class="fileupload-process"></span>
    </div>
    <!-- The table listing the files available for upload/download -->
    <table role="presentation" class="table"><tbody class="files"></tbody></table>
</form>
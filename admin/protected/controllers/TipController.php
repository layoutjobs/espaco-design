<?php

class TipController extends Controller
{		
	public $searchForm = true;
	
	public function actionIndex($s = null, $private = null)
	{
		$model = new Tip('search');
		$model->unsetAttributes();
	
		if (isset($_GET['Tip']))
			$model->setAttributes($_GET['Tip']);
		else 
			$model->s = $s;
	
		$this->render('index', array('model' => $model));
	}
	
	public function actionCreate()
	{
		$model = new Tip;

		if (isset($_POST['Tip'])) {
			$model->setAttributes($_POST['Tip']);

			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->redirect(array('update', 'id' => $model->id));
			}
		}

		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel('Tip');

		if (isset($_POST['Tip'])) {
			$model->setAttributes($_POST['Tip']);

			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->refresh();
			}
		}

		$this->render('update', array('model' => $model));
	}

	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$this->loadModel('Tip', $id)->delete();

			if (!Yii::app()->request->isAjaxRequest) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> O registro foi excluído com sucesso.');		

				$this->redirect(array('index'));
			}
		} else
			throw new CHttpException(400, 'Requisição inválida. Por favor, não repita esta requisição novamente.');
	}
}

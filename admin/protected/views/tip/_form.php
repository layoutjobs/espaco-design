<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->dropDownListControlGroup($model, 'published', array(1 => 'Publicado', 0 => 'Não Publicado')); ?>

<div class="control-group<?php echo $model->hasErrors('date') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'date', array('class' => 'control-label')); ?>
	<div class="controls">
		<div class="input-append">
			<?php $this->widget('yiiwheels.widgets.datepicker.WhDatePicker', array(
			    'model' => $model,
			    'attribute' => 'date',
				'pluginOptions' => array(
					'format' => 'dd/mm/yyyy',
					'language' => 'pt',
				),
			)); ?>
		    <span class="add-on"><?php echo TbHtml::icon(TbHtml::ICON_CALENDAR); ?></span>
		</div>
		<?php echo $form->error($model, 'date'); ?>
	</div>
</div>

<?php echo $form->textFieldControlGroup($model, 'title'); ?>

<div class="control-group<?php echo $model->hasErrors('content') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'content', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
		    'model' => $model,
		    'attribute' => 'content',
		)); ?>
		<?php echo $form->error($model, 'content'); ?>
	</div>
</div>

<div class="form-actions">
	<?php echo TbHtml::button('Salvar', array(
		'type' => 'submit',
		'color' => TbHtml::BUTTON_COLOR_PRIMARY,
		'icon' => TbHtml::ICON_OK,
	)); ?>	

	<?php echo TbHtml::linkButton('Cancelar', array(
		'type' => 'link',
		'url' => array('index'),
		'color' => TbHtml::BUTTON_COLOR_LINK,
	)); ?>
</div>
	
<?php $this->endWidget(); ?>
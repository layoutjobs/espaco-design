<?php 
$this->pageTitle = 'Produtos';
$this->menu = array(
	array(
		array(
			'label' => 'Incluir',
			'icon' => 'file',
			'url' => array('create'),
		),
	),
); ?>

<?php $this->widget('yiiwheels.widgets.grid.WhGroupGridView', array(
	'type' => array(TbHtml::GRID_TYPE_BORDERED),
	'dataProvider' => $model->search(),
	'extraRowColumns' => array('manufacturer_id'),
	'columns' => array(
		array(
			'name' => 'img',
			'type' => 'raw',
			'value' => 'CHtml::image($data->getImage("thumbnail")->url, null, array("style" => "height: 40px; width: auto;"))',
		),
		'name',
		array(
			'name' => 'categories',
			'value' => "implode(', ', CHtml::listData(\$data->categories, 'id', 'name'))",
		),
		array(
			'type' => 'html',
			'name' => 'description',
			'htmlOptions' => array('width' => '50%'),
		),
		array(
			'name' => 'manufacturer_id',
			'type' => 'raw',
			'value' => '$data->manufacturer ? $data->manufacturer->name : ""',
			'htmlOptions' => array('style' => 'display: none;'),
			'headerHtmlOptions' => array('style' => 'display: none;'),
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => "{update}\n{delete}",
		),
	),
)); ?>
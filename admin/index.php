<?php

chdir(__DIR__);

$environment = strpos($_SERVER['SERVER_NAME'], '.') !== false ? 'web' : 'local';
defined('APP_ENVIRONMENT') or define('APP_ENVIRONMENT', $environment);

if ($environment === 'local') {
	defined('YII_DEBUG') or define('YII_DEBUG', true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
}

$dir      = dirname(rtrim(__DIR__, DIRECTORY_SEPARATOR));
$root     = rtrim($_SERVER['DOCUMENT_ROOT'], DIRECTORY_SEPARATOR);
$dir      = str_replace('\\', '/', $dir);
$root     = str_replace('\\', '/', $root);
$alias    = ltrim(str_replace($root, '', $dir), '/');
$basePath = str_replace('/', DIRECTORY_SEPARATOR, ($root . '/' . $alias));
$baseUrl  = 'http://' . trim($_SERVER['HTTP_HOST'], '/') . '/' . $alias;

defined('APP_BASE_PATH') or define('APP_BASE_PATH', $basePath);
defined('APP_BASE_URL') or define('APP_BASE_URL', $baseUrl);
defined('APP_FILE_PATH') or define('APP_FILE_PATH', $basePath . DIRECTORY_SEPARATOR . 'files');
defined('APP_FILE_URL') or define('APP_FILE_URL', $baseUrl . '/files');
defined('APP_IMG_URL') or define('APP_IMG_URL', $baseUrl . '/img');
defined('APP_JS_URL') or define('APP_JS_URL', $baseUrl . '/js');

require_once('../common/vendor/yii/framework/yii.php');

Yii::createWebApplication("protected/config/main.php")->run();
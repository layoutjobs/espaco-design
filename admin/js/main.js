!function($) {

  "use strict";

  var $typeahead = $.fn.typeahead.Constructor.prototype

  $typeahead.extractor = function (query) {
    var result = /([^,]+)$/.exec(query)
    if (result && result[1])
        return result[1].trim()
    return ''
  }

  $typeahead.updater = function (item) {
    if (this.$element.attr('data-mode') == 'multiple')
      return this.$element.val().replace(/[^, ]*$/, '') + item + ', '
    else
      return item
  }

  $typeahead.matcher = function (item) {
    if (this.$element.attr('data-mode') == 'multiple') {
      var query = this.extractor(this.query);
      if (!query) return false;
      return ~item.toLowerCase().indexOf(query.toLowerCase())
    } else
      return ~item.toLowerCase().indexOf(this.query.toLowerCase())
  }

  $typeahead.highlighter = function (item) {
    if (this.$element.attr('data-mode') == 'multiple')
      var query = this.extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
    else
      var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
    return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
      return '<strong>' + match + '</strong>'
    })
  }

  $('button[rel="tooltip"]').tooltip(); 

  $('#ajax-loading')
    .bind('ajaxSend', function() {
      $('body').addClass('loading')
    })
    .bind('ajaxComplete', function() {
      $('body').removeClass('loading')
    })

}(window.jQuery);
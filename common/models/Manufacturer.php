<?php

class Manufacturer extends CActiveRecord
{	
	public $s;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{manufacturer}}';
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max' => 128),
			array('name', 'unique'),

			array('s', 'safe', 'on' => 'search'),
		);
	}

	public function defaultScope() 
	{
		$alias = $this->getTableAlias(false, false);
		return array('order' => "$alias.name");
	}
	
	public function attributeLabels()
	{
		return array(
			's' => 'Pesquisar',
			'id' => '#',
			'name' => 'Nome',
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		if ($this->s) {
			$criteria->compare('t.name', $this->s, true);				
		}

		return new CActiveDataProvider('Manufacturer', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 't.name',
			),
		));
	}
}
<?php

class Tip extends CActiveRecord
{	
	public $s;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{tip}}';
	}

	public function rules()
	{
		return array(
			array('title, content', 'required'),
			array('title', 'length', 'max' => 128),
			array('title', 'unique'),
			array('published', 'boolean'),
			array('published', 'default', 'value' => 1, 'setOnEmpty' => true),
			array('date', 'date', 'format' => Yii::app()->locale->dateFormat),
			array('date', 'default', 'value' => $this->formatDateTime('date', null), 'setOnEmpty' => true),
			array('content', 'filter', 'filter' => array($purifier = new CHtmlPurifier(), 'purify')),
			array('content', 'safe'),

			array('s', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function scopes()
	{
		return array(
			'published' => array(
				'condition' => 'published = 1',
			),
		);
	}

	public function defaultScope() 
	{
		$alias = $this->getTableAlias(false, false);
		return array('order' => "$alias.published, $alias.date DESC, $alias.title");
	}
	
	public function attributeLabels()
	{
		return array(
			's' => 'Pesquisar',
			'id' => '#',
			'title' => 'Título',
			'content' => 'Conteúdo',
			'excerpt' => 'Resumo',
			'published' => 'Publicado?',
			'date' => 'Data',
		);
	}

	public function behaviors()
	{
		return array(
			'aliasBehavior' => array(
				'class' => 'common.behaviors.AliasBehavior',
				'aliasOriginAttribute' => 'title',
			),
			'dateTimeBehavior' => array(
				'class' => 'common.behaviors.DateTimeBehavior',
			),
			'excerptBehavior' => array(
				'class' => 'common.behaviors.ExcerptBehavior',
				'excerptAttribute' => null,
			),
		); 
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		if ($this->s) {
			$criteria->compare('t.title', $this->s, true);
			$criteria->compare('t.content', $this->s, true, 'OR');							
		}

		return new CActiveDataProvider('Tip', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 't.published, t.date DESC, t.title',
			),
		));
	}
}
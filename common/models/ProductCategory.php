<?php

class ProductCategory extends CActiveRecord
{	
	public function tableName()
	{
		return '{{product_category}}';
	}

	public function relations()
	{
		return array(
		    'product'  => array(self::BELONGS_TO, 'Product', 'product_id'),
		    'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
		);
	}
}
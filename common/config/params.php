<?php

return array(
	'site.name' => 'Espaço Design',
	'site.keywords' => '',
	'site.description' => '',
	'biz.name' => 'Espaço Design Ambientes Planejados',
	'biz.phone' => '(11) 4098.1454',
	'biz.mobile' => '(11) 9.6778.1473',
	'map.lat' => '-23.1996398',
	'map.lng' => '-47.3023881',
	'mail.senderEmail' => null,
	'facebook.url' => 'https://www.facebook.com/espacodesign',
	'ga.trackingId' => null,

	'upload.imageVersions' => array(
		'thumbnail' => array(
			'max_width' => 240,
			'max_height' => 135,
		),
		'small' => array(
			'max_width' => 640,
			'max_height' => 360,
		),
		'medium' => array(
			'max_width' => 960,
			'max_height' => 540,
		),
		'large' => array(
			'max_width' => 1600,
			'max_height' => 900,
		),
	),
);
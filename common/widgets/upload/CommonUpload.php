<?php

class CommonUpload extends CWidget
{
	/**
	 * @var array of options to be encoded and passed to the Fileupload Javascript API.
	 */
	public $options;
		
	/**
	 * @var string the url to the upload handler.
	 */
	public $url;
		
	/**
	 * @var string the form view to be rendered.
	 */
	public $formView = 'form';
	
	/**
	 * @var string the upload view to be rendered.
	 */
	public $uploadView = 'upload';
	
	/**
	 * @var string the download view to be rendered.
	 */
	public $downloadView = 'download';
	
	/**
	 * Publishes the required assets.
	 */
	public function init()
	{		
		parent::init();
		$this->publishAssets();
	}
	
	/**
	 * Generates the required HTML and Javascript.
	 */
	public function run()
	{
		$options = $this->options;
		$options['autoUpload'] = true;
		$options = CJavaScript::encode($options);		
	
		$js = <<<JS
$('#{$this->id} form').fileupload({url: '{$this->url}'});
$('#{$this->id} form').fileupload({$options});
$('#{$this->id} form').addClass('fileupload-processing');
$.ajax({
    url: $('#{$this->id} form').fileupload('option', 'url'),
    dataType: 'json',
    context: $('#{$this->id} form')[0]
}).always(function () {
    $(this).removeClass('fileupload-processing');
}).done(function (result) {
    $(this).fileupload('option', 'done')
        .call(this, $.Event('done'), {result: result});
});
JS;

		Yii::app()->clientScript->registerScript($this->id, $js, CClientScript::POS_READY);
		
		echo '<div id="' . $this->id . '" class="fileupload">';

		if ($this->formView)
			$this->render($this->formView);			

		if ($this->uploadView)		
			$this->render($this->uploadView);

		if ($this->downloadView)		
			$this->render($this->downloadView);

		echo '</div>';
	}
	
	/**
	 * Publishes the required CSS and Javascript.
	 * @throws CHttpException if the assets folder was not found.
	 */
	public function publishAssets()
	{
		$assets  = dirname(__FILE__) . '/assets';
		$baseUrl = Yii::app()->assetManager->publish($assets);

		if (is_dir($assets)) {
			$csss = array('blueimp-gallery.min', 'jquery.fileupload', 'jquery.fileupload-ui');

			foreach ($csss as $css)
				Yii::app()->clientScript->registerCssFile($baseUrl . '/css/' . $css . '.css');

			$jss = array('vendor/jquery.ui.widget', 'tmpl.min', 'load-image.min', 'canvas-to-blob.min', 
				'jquery.blueimp-gallery.min', 'jquery.iframe-transport', 'jquery.fileupload', 
				'jquery.fileupload-process', 'jquery.fileupload-image', 'jquery.fileupload-audio', 
				'jquery.fileupload-video', 'jquery.fileupload-validate', 'jquery.fileupload-ui' 
			);

			foreach ($jss as $js)
				Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/' . $js . '.js', CClientScript::POS_END);

			// The localization script
			// Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/locale.js', CClientScript::POS_END);	

			/* 
			TODO: find a way to register these script files:

			<!-- CSS adjustments for browsers with JavaScript disabled -->
			<noscript><link rel="stylesheet" href="css/jquery.fileupload-noscript.css"></noscript>
			<noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>
			<!-- CSS adjustments for browsers with JavaScript disabled -->
			<noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>
			<!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
			<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->								
			<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
			<!--[if (gte IE 8)&(lt IE 10)]>
			<script src="js/cors/jquery.xdr-transport.js"></script>
			<![endif]--> 
			*/
		} else
			throw new CHttpException(500, __CLASS__ . ' - Error: Couldn\'t find assets to publish.');
	}
}
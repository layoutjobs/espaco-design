<?php

class CommonController extends CController
{	
	public $pageSubtitle;
	
	public $menu = array();

	public $breadcrumbs = array();

	public $searchForm = false;

	public $searchParams = array();

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel($modelClass, $id = null, $scenario = null)
	{	
		if ($scenario !== null)
			$modelClass::model()->scenario = $scenario;

		if ($id || isset($_GET['id']))
			$model = $modelClass::model()->findByPk($id ? $id : $_GET['id']);

		if ($model === null)
			throw new CHttpException(404, 'A página solicitada não existe.');

		return $model;
	}
}

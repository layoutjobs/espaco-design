<?php

class CommonImage extends CApplicationComponent
{
    public $librarySourcePath;
    public $params = array();

    public function init()
    {
        parent::init();
        Yii::import(rtrim($this->librarySourcePath . '.*') . '.*');
    }

    public function load($image)
    {
        $config = array(
            'driver' => 'GD',
            'params' => $this->params,
        );

        return new Image($image, $config);
    }
}
?>

<?php

class CommonUserIdentity extends CUserIdentity
{	
	private $_id;
	private $_owner;

	protected function findUser()
	{
		return User::model()->find('LOWER(username)=? OR LOWER(email)=?', array(strtolower($this->username), strtolower($this->username)));
	}

	public function authenticate()
	{
		$user = $this->findUser();
		
		if ($user === null)
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if (!$user->validatePassword($this->password))
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else {
			$this->_id = $user->id;
			$this->setState('name', $user->name);

			if ($user->type !== 'admin') {
				$this->setState('is' . ucfirst($user->type), true);
				$this->setState('type', $user->type);

				if ($user->owner !== null) {
					$this->_owner = $user->owner;
					$this->setState('ownerId', $user->owner_id);
				}
			} else 
				$this->setState('isAdmin', true);

			$this->errorCode = self::ERROR_NONE;
		}
		return $this->errorCode == self::ERROR_NONE;
	}
	
	public function getId()
    {
        return $this->_id;
    }

	public function getOwner()
    {
    	if ($this->isGuest || $this->_owner === false || !$this->getState('ownerClass', false) || !$this->getState('ownerId', false))
    		return null;

    	if ($this->_owner === null) {
			$class = $this->getState('ownerClass');
			$owner = $class::model()->findByPk($this->getState('ownerId'));

			if ($owner === null)
				$this->_owner = false;
			else
				$this->_owner = $owner;	
    	}
    	
    	return $this->_owner;
    }
}
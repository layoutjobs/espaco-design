<?php

class AliasBehavior extends CActiveRecordBehavior
{
	public $aliasAttribute = 'alias';
	public $aliasOriginAttribute = 'name';

	public function beforeSave($event)
	{
		$this->owner->{$this->aliasAttribute} = $this->createAlias($this->owner->{$this->aliasOriginAttribute});
	}

	public function createAlias($origin) 
	{
		// replace imcompatible caracters
		$alias = strtr(utf8_decode($origin), utf8_decode('áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ'), 'aaaaeeiooouucAAAAEEIOOOUUC');
		// replace non letter or digits by -
		$alias = preg_replace('~[^\\pL\d]+~u', '-', $alias);
		// trim
		$alias = trim($alias, '-');
		// transliterate
		$alias = iconv('utf-8', 'us-ascii//TRANSLIT', $alias);
		// lowercase
		$alias = strtolower($alias);
		// remove unwanted characters and return
		return preg_replace('~[^-\w]+~', '', $alias);	
	}
}

<?php

class PositionBehavior extends CActiveRecordBehavior
{
	public $positionAttribute = 'position';

	public function beforeSave($event)
	{
		$this->owner->{$this->positionAttribute} = $this->parsePosition($this->owner->{$this->positionAttribute});
	}

	public function parsePosition($position)
	{
		if (is_array($position))
			return implode(', ', $position);	
		else
			return $position;
	}

	public function getPositionAsArray()
	{
		if (!is_array($this->owner->{$this->positionAttribute})) {
			$positions = explode(',', $this->owner->{$this->positionAttribute});
			$positions = array_map('trim', $positions);
			return $positions;		
		}
	}

	public function getPositionOptions()
	{
		return Yii::app()->params['site.positions'];	
	}

	public function position($position)
	{
		$alias = $this->owner->tableAlias;
		$this->owner->dbCriteria->mergeWith(array(
			'condition' => "$alias.$this->positionAttribute REGEXP ',?$position,?'",	
		));

		return $this->owner;
	}
}

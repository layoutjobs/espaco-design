<?php

class ImageBehavior extends CActiveRecordBehavior
{
	private $_oldImg;

	public $imgAttribute = 'img';

	public function afterFind($event)
	{
		$this->_oldImg = $this->owner->{$this->imgAttribute};
	}

	public function beforeSave($event)
	{
		$this->saveImage();
	}

	public function afterDelete($event)
	{
		$this->deleteImage();
	}

	public function getImage($version = null)
	{
		if (!$this->owner->{$this->imgAttribute})
			return;

		$url = APP_FILE_URL . '/' . (strtolower(get_class($this->owner))) . ($version !== null ? '/' . $version : null) . '/' . $this->owner->{$this->imgAttribute};

		return new CAttributeCollection(array(
			'name' => $this->owner->{$this->imgAttribute},
			'url' => $url,
		));
	}

	public function saveImage()
	{
		if ($this->owner->{$this->imgAttribute} && $this->owner->{$this->imgAttribute} instanceof CUploadedFile) {
			$this->deleteImage();
			$imgName = uniqid() . '.' . $this->owner->{$this->imgAttribute}->extensionName;
			$basePath = APP_FILE_PATH . DIRECTORY_SEPARATOR . strtolower(get_class($this->owner));

			if (!is_dir($basePath)) 
				mkdir($basePath, 0700);

			$imgPath = $basePath . DIRECTORY_SEPARATOR . $imgName;
			$this->owner->{$this->imgAttribute}->saveAs($imgPath);
			$this->owner->{$this->imgAttribute} = $imgName;	
		
			foreach (Yii::app()->params['upload.imageVersions'] as $version => $params) {
				$versionPath = $basePath . DIRECTORY_SEPARATOR . $version;

				if (!is_dir($versionPath)) 
					mkdir($versionPath, 0700);

				$versionImgPath = $versionPath . DIRECTORY_SEPARATOR . $imgName;
				$image = Yii::app()->image->load($imgPath);	
				$image->resize($params['max_width'], $params['max_height']);
				$image->save($versionImgPath);
			}
		}
	}

	public function deleteImage()
	{
		$basePath = APP_FILE_PATH . DIRECTORY_SEPARATOR . strtolower(get_class($this->owner));
		$imgPath = $basePath . DIRECTORY_SEPARATOR . $this->_oldImg;
		
		foreach (Yii::app()->params['upload.imageVersions'] as $version => $params) {
			$versionPath = $basePath . DIRECTORY_SEPARATOR . $version . DIRECTORY_SEPARATOR . $this->_oldImg;
			
			if (is_file($versionPath)) 
				unlink($versionPath);
		}
		
		if (is_file($imgPath)) 
			unlink($imgPath);
	}
}
